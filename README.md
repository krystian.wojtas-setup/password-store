## Purpose

Keep password store subdirectories in separate git repositories and install only
needed ones.

## Setup

### Server

It is expected to store all subdirectories on origin on same path level as this repository.
```
% ls -a1
common.git
openwrt.git
.password-store.git
rock64.git
```

### Client

#### Initialize

Go to branch with proper defined set of subdirectories
``` sh
git checkout zbook
```

Clone
``` sh
./clone
```

#### Push

Push current changes in all subdirectories
``` sh
./push
```

#### Pull

Pull latest changes from all subdirectories
``` sh
./pull
```

#### Import keys

Import public and private key

``` sh
ssh $SERVER gpg --export $ID | gpg --import
ssh $SERVER gpg --export-secret-keys $ID | gpg --import
```

Trust it
``` sh
gpg --edit-key $ID trust
5
y
q
```
